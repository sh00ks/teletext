# README #

The Teletext system written in C/SDL 2.0.

### How do I get set up? ###

* gcc to compile. 
* Libraries *libsdl2-dev, libsdl2-ttf-dev, libncurses5-dev*
* Command *gcc teletext.c CuTest.c teletext_test.c -lSDL2main -lSDL2 -lSDL2_ttf -lm -lncurses -o Teletext.exe*
* Load .m7 files as an **optional** second argument *Teletext.exe panda.m7*.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* mrjoshuaali@yahoo.co.uk